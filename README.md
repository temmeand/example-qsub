Example Qsub Files
==================

This repository contains example qsub files. Most have been written or put
together by me. Please feel free to re-use with attribution. 

These two qsub's go together:

    callable.qsub
    submitJob.sh

The `submitJob` script calls `callable.qsub`.

These two qsub's go together:

    loop-cont.qsub
    loop-init.qsub

The `loop-init.qsub` is the first qsub file. It submits a dependent job that
will continue solving when the walltime is reached. `loop-cont` continues and
re-submits itself when `loop-init` gets killed.


Repo Availability/Locations
---------------------------

This repository is mirrored on https://bitbucket.org/temmeand/.
      
Errors/Problems
---------------

Please contact me or submit a pull request if you find an error. Thank you.
