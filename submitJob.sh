qsub -N ${1%.hfss} -l walltime=0:59:00,mem=05gb,nodes=4:ppn=1 ./callable.qsub
# Use this script to submit a job. It calls a qsub that has all the nitty gritty
# details in it. This allows for easy modification of the job resources without
# messing with the rest of the qsub file.
